> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Mobile Web Application Development- LIS4381

## Humphrey Minott

### Assignment 2

#### README.md file should include the following items:

* Create strings.xml file in android studio
* Make another activity_main.xml file called activity_recipe.xml
* Add textview widgets to design
* Screenshot of running application’s first user interface;
* Screenshot of running application’s second user interface;

#### Assignment Screenshots:

*Screenshot of running application’s first user interface*:

![First interface screenshot](img/firstInterface.png)

*Screenshot of running application's second user interface*:

![Second interface screenshot](img/secondInterface.png)
