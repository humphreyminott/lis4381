> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Mobile Web Application Development- LIS4381

## Humphrey Minott

### Assignment 5

#### README.md file should include the following items:

* Copy index.php file
* CSS Folder
* Fonts Folder
* Javascript Folder
* Bootstrap for HTML
* Placed files in A5 subfolder
* Screenshot of server-side validation and index file
* Data for petstore

#### Assignment Screenshots:

*Screenshot of localhost A5 index*:

![index file](img/img1.png)

*Screenshot of server-side validation*:

![petstore process](img/img2.png)