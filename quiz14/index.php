<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Fri, 06-24-16, 20:09:09 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through LIS4381 Web App Development class.">
	<meta name="author" content="Humphrey minott">
	<link rel="icon" href="favicon.ico">

	<title>Simple Employee Class</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

<!-- jQuery DataTables //-->
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.7/css/dataTables.responsive.css"/>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">	
			<div class="page-header">
				<?php include_once("global/header.php"); ?>	
			</div>
            
            <h2>Simple Employee Class</h2>
			<form role="form" method="post" class="form-horizontal" action="process.php">
								
                                <div class="form-group">
										<label class="col-sm-2 control-label" for="type">First Name:</label>
										<div class="col-sm-10">
												<input type="text" class="form-control" name="fname" id="fname" placeholder="Enter first name" />
										</div>
								</div>

								<div class="form-group">								
										<label class="col-sm-2 control-label" for="length">Last Name:</label>
										<div class="col-sm-10">
												<input type="text" class="form-control" name="lname" id="lname" placeholder="Enter last name" />
										</div>
								</div>

								<div class="form-group">								
										<label class="col-sm-2 control-label" for="length">SSN:</label>
										<div class="col-sm-10">
												<input type="text" class="form-control" name="ssn" id="ssn" placeholder="Enter ssn" />
										</div>
								</div>
								
								<div class="form-group">
										<label class="col-sm-2 control-label" for="age">Age:</label>
										<div class="col-sm-10">
										<select id= "age" name="age">
											<?php
											for($i=18; $i<=65;$i++)
											{
												echo "<option value=".$i.">".$i."</option>";
											}
											?>
										</select>
										</div>
										</div>

									<div class="form-group">								
										<label class="col-sm-2 control-label" for="age">Gender:</label>
										<div class="col-sm-10">
										<label class="radio-inline"><input type="radio" name="gender" value="male">Male</label>
										<label class="radio-inline"><input type="radio" name="gender" value="Female">Female</label>
										</div>
								</div>
                                
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-default">Submit</button>
                                    </div>
                                </div>
            </form>
            
<?php include_once "global/footer.php"; ?>
        </div>
    </div>
    
    	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<!-- Turn off client-side validation, in order to test server-side validation.  -->
<script type="text/javascript" src="js/formValidation/formValidation.min.js"></script>

<!-- Note the following bootstrap.min.js file is for form validation, different from the one above. -->
<script type="text/javascript" src="js/formValidation/bootstrap.min.js"></script>

<script type="text/javascript" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>
    
    <script>
        $(document).ready(function(){
            $('#myTable').DataTable({
                responsive: true
            });
        });
    </script>
</body>
</html>