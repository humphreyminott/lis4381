<?php
    class Employee extends Person
    {
        private $ssn;
        private $gender;
        
        public function __construct($fn = "John", $ln = "Doe", $ag = 21, $s= '123456789', $g= 'male')
        {
            $this->ssn = $s;
            $this->gender = $g;
            
            parent::__construct($fn, $ln, $ag);
            echo("Creating <strong>" . person::GetFname() . " " . person::GetLname() . " is " . person::GetAge() . "with ssn: " . $this->ssn . "and is " . $this->gender . "</strong> emloyee object from parameterized constructor (accepts five arguments): <br />");
        }
        
        function __destruct()
        {
            parent::_destruct();
            echo("Destroying <strong>" . person::GetFname() . " " . person::GetLname() . " is " . person::GetAge() . "with ssn: " . $this->ssn . "and is " . $this->gender . "</strong> emloyee object.<br/>");
        }
        
        //setter methods
        public function SetSSN($s = "111111111")
        {
            $this->ssn = $s;
        }
        
        public function SetGender($g = "f")
        {
            $this->gender = $g;
        }

        //getter methods
        public function GetSSN()
        {
            return $this->ssn;
        }
        
        public function GetGender()
        {
            return $this->gender;
        }
    }
?>