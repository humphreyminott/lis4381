> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Mobile Web Application Development- LIS4381

## Humphrey Minott

### Assignment 4

#### README.md file should include the following items:

* Copy index.php file
* CSS Folder
* Fonts Folder
* Javascript Folder
* Bootstrap for HTML
* Placed files in A4 subfolder
* Screenshot of localhost homepage for LIS4381 Repo;
* Screenshot of assignment 4 client-side validation;
* Screenshot of assignment 4 correct input for validation

#### Assignment Screenshots:

*Screenshot of localhost homepage for LIS4381 Repo*:

![homepage screenshot](img/homepage.png)

*Screenshot of assignment 4 client-side validation*:

![assignment 4 screenshot](img/code_validation.png)

*Screenshot of assignment 4 correct input for validation*:

![Second interface screenshot](img/correct_input.png)
