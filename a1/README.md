> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Mobile Web Application Development- LIS4381

## Humphrey Minott

### Assignment 1

Four parts

1. Distributed version control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (BitbucketStationLocations and myteamquotes)

#### README.md file should include the following items:

* Bullet-list items
* Screenshot of AMPPS application running
* Screenshot running JDK java Hello
* Screenshot of running Android Studio My First App

> #### Git commands w/short descriptions:

1. Git init- Initializes a git repository â€“ creates the initial â€˜.gitâ€™ directory in a new or in an existing project.
2. Git Status- Shows you the status of files in the index versus the working directory.
3. Git add- Adds files changes in your working directory to your index.
4. Git commit- Takes all of the changes written in the index, creates a new commit object pointing to it and sets the branch to point to that new commit.
5. Git push- Pushes all the modified local objects to the remote repository and advances its branches.
6. Git pull- Fetches the files from the remote repository and merges it with your local one
7. Git clone- Makes a Git repository copy from a remote source. Also adds the original location as a remote so you can fetch from it again and push to it if you have permissions.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/AMPPS.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/HelloWorld.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/MyFirstApp.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/humphreyminott/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/humphreyminott/myteamquotes/ "My Team Quotes Tutorial")