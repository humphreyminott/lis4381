<?php
//show errors: at least 1 and 4...
ini_set('display_errors', 1);
//ini_set('log_errors', 1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);

//use for inital test of form inputs
//exit(print_r($_POST));
?>
<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Fri, 06-24-16, 20:09:09 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through LIS4381 Web App Development class.">
	<meta name="author" content="Humphrey Minott">
	<link rel="icon" href="favicon.ico">

	<title>Simple Person Class</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

<!-- jQuery DataTables //-->
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.7/css/dataTables.responsive.css"/>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">	
			<div class="page-header">
				<?php include_once("global/header.php"); ?>	
			</div>
            
<?php
require_once("classes/person.php");

//get user input
$personFname = $_POST['fname'];
$personLname = $_POST['lname'];
$personAge = $_POST['age'];

//instantiate person objects from constructor accepting 2 args
$person1 = new Person();
$person2 = new Person($personFname, $personLname, $personAge);
?>

        <h2>Simple Person Class</h2>
        
<div class="table-responsive">
<table id="myTable" class="table table-striped table-condensed" >
<thead>
    <tr>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Age</th>
    </tr>
</thead>
<tr>
    <td><?php echo $person1->GetFname(); ?></td>
    <td><?php echo $person1->GetLname(); ?></td>
    <td><?php echo $person1->GetAge(); ?></td>
</tr>

<tr>
    <td><?php echo $person2->GetFname(); ?></td>
    <td><?php echo $person2->GetLname(); ?></td>
    <td><?php echo $person2->GetAge(); ?></td>
</tr>
</table>

    <?php include_once "global/footer.php"; ?>
</div>
        </div>
    </div>

    	<!-- Bootstrap JavaScript
	================================================== -->
	<!-- Placed at end of document so pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

<!-- Turn off client-side validation, in order to test server-side validation.  -->
<script type="text/javascript" src="js/formValidation/formValidation.min.js"></script>

<!-- Note the following bootstrap.min.js file is for form validation, different from the one above. -->
<script type="text/javascript" src="js/formValidation/bootstrap.min.js"></script>

<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>

	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="js/ie10-viewport-bug-workaround.js"></script>
    
    <script>
        $(document).ready(function(){
            $('#myTable').DataTable({
                responsive: true
            });
        });
    </script>
</body>
</html>