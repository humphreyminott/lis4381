> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web Application Development

## Humphrey Minott

### LIS4381 Requirements:

*Course Work Links*

1. [A1 README.md](a1/README.md)
    * Install AMPPS
    * Install JDK
    * Install Android Studio and create My First App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorial (bitbucketstationlocations and myteamquotes)
    * Provide git command descriptions

2. [A2 README.md](a2/README.md)
    * Provide Bitbucket read-only access to lis4381 repo
    * Provide screenshot of Bruschetta Recipe application

3. [A3 README.md](a3/README.md)
    * Connect to LocalHost Server
    * Create EER Diagram
    * Include 10 unique records per table for diagram
    * Forward-engineering the ERD data design with data
    * Create Ticket application in Android Studio
    * Provide screenshots
4. [A4 README.md](a4/README.md)
    * Copy assignment starter files from Bitbucket repo
    * Edit index.php file, changing title, navigation links, and header tags
    * Add Jquery validation and regular expressions
    * Edit global header and footer file
    * Provide screenshots
5. [A5 README.md](a5/README.md)
    * add server side validation for database entity
    * allow users to add petstore
    * browse create, user data
    * Provide screenshots
6. [P1 README.md](p1/README.md)
    * Create a Business Card
    * Create a launcher icon image and display it in both activities (screens)
    * Must add background color(s) to both activities
    * Must add border around image and button
    * Must add text shadow (button)
    * Provide screenshots
7. [P2 README.md](p2/README.md)
    * add the Edit and Delete petstore file for the database
    * PHP server-side validation to help prevent SQL injections
    * Allow users to edit data inputted
    * allow user to delete data inputted
    * Create RSS Feed
    * Provide screenshots