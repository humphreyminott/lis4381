> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Mobile Web Application Development- LIS4381

## Humphrey Minott

### Assignment 3

#### README.md file should include the following items:

* Created Diagram from MySQL WorkBench
* Screenshot of ERD
* Created Ticket application from Android Studio
* Edit the MainActivity.java file in Android Studio
* Screenshot of running application's first user interface;
* Screenshot of running application's second user interface;
* Links to the following files:
 a. [sql file](a3.sql)
 b. [mwb file](a3.mwb)
* Screenshot of running application’s second user interface;

#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD](a3.png)

*Screenshot of running application’s first user interface*:

![First interface screenshot](UserInterface1.png)

*Screenshot of running application's second user interface*:

![Second interface screenshot](UserInterface2.png)


