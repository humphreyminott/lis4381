> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Mobile Web Application Development- LIS4381

## Humphrey Minott

### Project 2

#### README.md file should include the following items:

* Copy assignment 5 folder
* CSS Folder
* Fonts Folder
* Javascript Folder
* Bootstrap for HTML
* Add the edit and delete petstore files
* confirm server-side validation
* Update data for petstore
* Create RSS Feed

#### project Screenshots:

*Screenshot of index file- database*:

![index file](img/img1.png)

*Screenshot of edit for petstore*:

![edit petstore](img/img2.png)

*Screenshot of project 2 server-side validation error*:

![error](img/img3.png)

*Screenshot of home page carousel*:

![home page](img/img4.png)

*Screenshot of RSS Feed*:

![rss feed](img/img5.png)