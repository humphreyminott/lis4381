> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Mobile Web Application Development- LIS4381

## Humphrey Minott

### Project 1

#### README.md file should include the following items:

* Created Business card application
* Screenshot of running application's first user interface
* Screenshot of running application's second user interface
* Image of Humphrey Minott

#### Assignment Screenshots:

*Screenshot of running application’s first user interface*:

![First interface screenshot](interface1.png)

*Screenshot of running application's second user interface*:

![Second interface screenshot](interface2.png)


